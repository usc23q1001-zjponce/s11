from django.urls import path
from . import views

app_name = 'todolist'

urlpatterns = [
    path('',views.index, name="index"),
    path('task/<int:todoitem_id>/', views.todoitem, name = 'viewtodoitem'),
    path('register', views.register, name='register'),
    path('update_profile', views.update_profile, name="update_profile"),
    path('login', views.login_view, name="login"),
    path('logout', views.logout_view, name="logout"),
    path('add_task', views.add_task, name="add_task"),
    path('<int:todoitem_id>/edit', views.update_task, name="update_task"),
    path('<int:todoitem_id>/delete', views.delete_task, name="delete_task"),
    path('eventAdd', views.eventAdd, name="eventAdd"),
    path('event/<int:viewevent_id>/', views.eventname, name = 'viewevent'),


]