from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
# Create your views here.
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from django.utils import timezone

from .models import ToDoItem, ToDoItemEvent
from .forms import LoginForm, AddTaskForm, UpdateTaskForm, RegisterForm, AddEventForm, UpdateProfileForm

def index(request):
    # return HttpResponse("Hello from the views.py file")
    # todoitem_list = ToDoItem.objects.all()
    # output = ', '.join([todoitem.task_name for todoitem in todoitem_list])
    # return HttpResponse(output)
    todoitem_list = ToDoItem.objects.filter(user_id=request.user.id)
    event_list = ToDoItemEvent.objects.filter(user_id=request.user.id)
    # todoitem_list2 = ToDoItem2.objects.all()
    context = {
        'todoitem_list': todoitem_list,
        'event_list,': event_list,
        'user': request.user
    }

    return render(request, "todolist/index.html", context)


def todoitem(request, todoitem_id):
    # response = "You are viewing the details of %s"
    # return HttpResponse(response % todoitem_id)
    # todoitem = model_to_dict(ToDoItem.objects.get(pk=todoitem_id))
    todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)

    # return render(request, "todolist/todoitem.html", todoitem)
    return render(request, "todolist/todoitem.html", model_to_dict(todoitem))


def register(request):
    # if request.method == 'POST':
    #     form = RegisterForm()

    # return render(request, "todolist/register.html", { 'form': form})

    # if request.method == 'GET':
    #     form  = RegisterForm()
    #     context = {'form': form}
    #     return render(request, "todolist/register.html", context)
    if request.method == 'POST':
        form  = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('todolist:login')
    else:
        form = RegisterForm()
    return render(request, "todolist/register.html", {'form': form})
    # else:
    #     print('Form is not valid')
    #     context = {'form': form}
    #     return render(request, "todolist/register.html", context)

   

def update_profile(request):
    if request.method == 'POST':
        form = UpdateProfileForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('todolist:index')
    else:
        form = UpdateProfileForm(instance=request.user)
    return render(request, 'todolist/update_profile.html', {'form': form})

def login_view(request):

    context = {}

    if request.method == 'POST':
        form = LoginForm(request.POST)

        if form.is_valid() == False:
            form = LoginForm()

        else:
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            context = {
                "username": username,
                "password": password,
            }

            if user is not None:
                login(request, user)
                return redirect("todolist:index")
            else:
                context = {
                    "error": True
                }
    return render(request, "todolist/login.html", context)


    # username ="johndoe"
    # password = "johndoe1"
    # user = authenticate(username=username, password=password)
    # print(user)

    # if user is not None:
    #     login(request, user)
    #     return redirect("todolist:index")
    
    # else: 
    #     return render(request, "todolist/login.html", context)


def logout_view(request):
    logout(request)
    return redirect("todolist:index")

def add_task(request):

    context = {}
    if request.method == 'POST':
        form = AddTaskForm(request.POST)

        if form.is_valid() == False:
            form = AddTaskForm()

        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']

            duplicates = ToDoItem.objects.filter(task_name=task_name)

            if not duplicates:
                ToDoItem.objects.create(task_name=task_name, description=description, date_created=timezone.now(),user_id=request.user.id)
                return redirect("todolist:index")
            
            else:
                context = {
                    "error": True
                }

    return render(request, "todolist/add_task.html", context)

def update_task(request, todoitem_id):

    todoitem = ToDoItem.objects.filter(pk=todoitem_id)

    context = {
        "user": request.user,
        "todoitem_id": todoitem_id,
        "task_name": todoitem[0].task_name,
        "desciption": todoitem[0].description,
        "status": todoitem[0].status
    }

    if request.method == 'POST':
        form = UpdateTaskForm(request.POST)

        if form.is_valid() == False:
            form = UpdateTaskForm()
        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']
            status = form.cleaned_data['status']

            if todoitem:
                todoitem[0].task_name = task_name
                todoitem[0].description = description
                todoitem[0].status = status

                todoitem[0].save()
                return redirect("todolist:index")
            else:
                context = {
                    "error": True
                }
    return render(request, "todolist/update_task.html", context)        

def delete_task(request, todoitem_id):
    todoitem = ToDoItem.objects.filter(pk=todoitem_id).delete()
    return redirect("todolist:index")

def eventAdd(request):

    context = {}
    if request.method == 'POST':
        form = AddEventForm(request.POST)

        if form.is_valid() == False:
            form = AddEventForm()

        else:
            event_name = form.cleaned_data['event_name']
            description = form.cleaned_data['description']

            duplicates = ToDoItemEvent.objects.filter(event_name=event_name)

            if not duplicates:
                ToDoItemEvent.objects.create(event_name=event_name, description=description, date_created=timezone.now(),user_id=request.user.id)
                return redirect("todolist:index")
            
            else:
                context = {
                    "error": True
                }

    return render(request, "todolist/eventAdd.html", context)

def eventname(request, eventname_id):
    # response = "You are viewing the details of %s"
    # return HttpResponse(response % todoitem_id)
    # todoitem = model_to_dict(ToDoItem.objects.get(pk=todoitem_id))
    eventname = get_object_or_404(ToDoItemEvent, pk=eventname_id)

    # return render(request, "todolist/todoitem.html", todoitem)
    return render(request, "todolist/todoitem2.html", model_to_dict(eventname))

